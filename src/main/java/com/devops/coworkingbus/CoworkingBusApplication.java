package com.devops.coworkingbus;

import com.devops.coworkingbus.model.CoworkingBus;
import com.devops.coworkingbus.repository.CoworkingBusRepository;
import com.thedeanda.lorem.LoremIpsum;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Random;
import java.util.stream.Stream;

@SpringBootApplication
public class CoworkingBusApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoworkingBusApplication.class, args);
	}

	@Bean
	ApplicationRunner init(CoworkingBusRepository repository) {
		return args -> {
			Stream.of("Ferrari", "Jaguar", "Bugatti")
					.forEach(name -> {
				CoworkingBus coworkingBus = new CoworkingBus();
				coworkingBus.setName(name);
				coworkingBus.setCapacity(new Random().nextInt(100));
				coworkingBus.setDescription(LoremIpsum.getInstance().getWords(10, 20));
				repository.save(coworkingBus);
				repository.findAll().forEach(System.out::println);
			});
		};
	}
}
