package com.devops.coworkingbus.repository;

import com.devops.coworkingbus.model.CoworkingBus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CoworkingBusRepository extends JpaRepository<CoworkingBus, Long> {

}
