'use strict';

class SearchButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    searchBuses() {
        fetch("/coworkingBuses")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result._embedded.coworkingBuses
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } if (isLoaded) {
            return (
                <ul>
                    {items.map(item => (
                        <li key={item.name}>
                            <img src={'images/bus-' + item.name + '.jpg'}/>
                            <div className='details'>
                                <p className='name'> {item.name}</p>
                                <p className='capacity'>Capacity: {item.capacity}</p>
                                <p className='description'>{item.description}</p>
                            </div>
                        </li>
                    ))}
                </ul>
            );
        } else {
            return <button onClick={() => this.searchBuses()}>Search for bus around me</button>
        }
    }
}

const domContainer = document.querySelector('#bus_list_container');
ReactDOM.render(<SearchButton/>, domContainer);