FROM openjdk:17-alpine AS java-build
# from https://linuxtut.com/how-to-make-docker-image-of-spring-boot-smaller-51713/
WORKDIR /jlink
ENV PATH $JAVA_HOME/bin:$PATH
RUN jlink --strip-java-debug-attributes --no-header-files --no-man-pages --compress=2 --module-path $JAVA_HOME --add-modules java.base,java.desktop,java.instrument,java.management.rmi,java.naming,java.prefs,java.scripting,java.security.jgss,java.sql,jdk.httpserver,jdk.unsupported --output jre-min

FROM alpine:3.12.0
USER root
COPY --from=java-build /jlink/jre-min /opt/jre-min
# todo use arg $REVISION in CI
# ARG REVISION
# COPY target/app-${REVISION}.jar app.jar
COPY target/*.jar /opt/app/app.jar
ENV PATH /opt/jre-min/bin:$PATH
EXPOSE 8080
WORKDIR /
CMD ["java", "-jar", "/opt/app/app.jar"]